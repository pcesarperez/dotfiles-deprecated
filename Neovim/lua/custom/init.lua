-- Mappings.
local map = require("core.utils").map

map("n", "<leader>cc", ":Telescope <CR>")
map("n", "<leader>q", ":q <CR>")

-- General options.

-- Forces English language.
vim.api.nvim_exec ("language en_US", true)

-- Disable the block cursor which messes up our terminal default cursor.
vim.opt.guicursor = ""

-- Remove most of the automatic formatting options (see `:help fo-table` for details).
vim.opt.formatoptions = "ql"