local M = {}

M.options = {
    tabstop = 8
}

M.ui = {
    theme = "nord",
}

return M
