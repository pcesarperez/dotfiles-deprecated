# https://askubuntu.com/a/555336/1451518
# https://stackoverflow.com/a/64194186/4491468

import gi

gi.require_version ("Wnck", "3.0")
gi.require_version ("Gtk", "3.0")

from gi.repository import Gtk, Wnck, GdkX11, Gdk

screen = Wnck.Screen.get_default ( )
screen.force_update ( )
windows = screen.get_windows ( )

for window in windows:
    if (window.get_class_instance_name ( ) == 'code'):
        break;

if window.is_minimized ( ):
    now = GdkX11.x11_get_server_time (GdkX11.X11Window.lookup_for_display (Gdk.Display.get_default ( ), GdkX11.x11_get_default_root_xwindow ( )))
    window.activate (now)
    window.maximize ( )
else:
    if window.is_active ( ):
        window.minimize ( )
    else:
        now = GdkX11.x11_get_server_time (GdkX11.X11Window.lookup_for_display (Gdk.Display.get_default ( ), GdkX11.x11_get_default_root_xwindow ( )))
        window.activate (now)