# Default PowerShell profile.


# Environment variables.
$env:SCOOP_APPS = "$env:USERPROFILE/scoop/apps"


# Updates `PSGallery` repository trust level, if needed.
if ((-not (Get-Module -ListAvailable -Name Color) -or (-not (Get-Module -ListAvailable -Name PSWriteColor)))) {
    if ((Get-PSRepository -Name PSGallery).InstallationPolicy -eq "Untrusted") {
        Write-Host "Updating " -ForegroundColor Cyan -NoNewline
        Write-Host "PSGallery" -ForegroundColor Yellow -NoNewline
        Write-Host " repository trust level..." -ForegroundColor Cyan -NoNewline
        Set-PSRepository -Name PSGallery -InstallationPolicy Trusted
        Write-Host "[OK]" -ForegroundColor Green
    }
}


# Installs/imports the `Color` module (colored `dir`).
if (Get-Module -ListAvailable -Name Color) {
    Import-Module -Name Color
} else {
    Write-Host "Installing module " -ForegroundColor Cyan -NoNewline
    Write-Host "Color" -ForegroundColor Yellow -NoNewline
    Write-Host "..." -ForegroundColor Cyan -NoNewline
    Install-Module -Name Color -AllowClobber -Scope CurrentUser
    Write-Host "[OK]" -ForegroundColor Green
Install-Module -Name Color -AllowClobber -Scope CurrentUser
}


# Installs/imports the `PSWriteColor` module (enhanced `Write-Host`).
if (Get-Module -ListAvailable -Name PSWriteColor) {
    Import-Module -Name PSWriteColor
} else {
    Write-Host "Installing module " -ForegroundColor Cyan -NoNewline
    Write-Host "PSWriteColor" -ForegroundColor Yellow -NoNewline
    Write-Host "..." -ForegroundColor Cyan -NoNewline
    Install-Module -Name PSWriteColor -AllowClobber -Scope CurrentUser
    Write-Host "[OK]" -ForegroundColor Green
}


# Predictive IntelliSense.
Set-PSReadLineOption -PredictionSource History


# Remaps Ctrl + d to exit the session, Unix style.
# @see https://stackoverflow.com/questions/8360215/use-ctrl-d-to-exit-and-ctrl-l-to-cls-in-powershell-console
# @see https://docs.microsoft.com/en-us/powershell/module/psreadline/set-psreadlinekeyhandler?view=powershell-7
Set-PSReadlineKeyHandler -Key ctrl+d -Function ViExit


# Remaps Ctrl + u to clear the current line, Unix style.
# @see https://stackoverflow.com/questions/8360215/use-ctrl-d-to-exit-and-ctrl-l-to-cls-in-powershell-console
# @see https://docs.microsoft.com/en-us/powershell/module/psreadline/set-psreadlinekeyhandler?view=powershell-7
Set-PSReadlineKeyHandler -Key ctrl+u -Function RevertLine


# Unix-style `nohup` alias.
function Start-HiddenProcess {
    param (
        $ProcessName
    )

    Start-Process -WindowStyle Hidden -FilePath $ProcessName
}
Set-Alias -Name nohup -Value Start-HiddenProcess


# Opens an administrator window, `su` style.
function Open-ElevatedTerminal {
    Start-Process -Verb RunAs -FilePath "wt"
}
Set-Alias -Name su -Value Open-ElevatedTerminal


# Runs a program with elevated privileges, `sudo` style.
function Start-ElevatedProcess {
    Start-Process @args -Verb RunAs
}
Set-Alias -Name sudo -Value Start-ElevatedProcess


# A faster way to halt (hibernate) the system.
function Stop-System {
    shutdown /h
}
Set-Alias -Name halt -Value Stop-System


# A faster way to reboot the system.
function Reset-System {
    shutdown /r /t 0
}
Set-Alias -Name reboot -Value Reset-System


# Loads zoxide (`cd` replacement).
Invoke-Expression (& {
    $hook = if ($PSVersionTable.PSVersion.Major -lt 6) { 'prompt' } else { 'pwd' }
    (zoxide init --hook $hook powershell | Out-String)
})


# Loads Starship (nice prompt).
Invoke-Expression (&starship init powershell)


# Sets the default folder pointing to the user home.
Set-Location $home