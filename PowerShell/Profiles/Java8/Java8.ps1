# Java 8 PowerShell profile.


# Enviroment variables.
$env:JAVA_HOME = "$env:SCOOP_APPS/openjdk8-redhat/current"
$env:ECLIPSE_HOME = "$env:SCOOP_APPS/eclipse-jee/current"
$env:IDEA_HOME = "$env:SCOOP_APPS/idea/current"
$env:GRADLE_HOME = "$env:SCOOP_APPS/gradle/current"
$env:MAVEN_HOME = "$env:SCOOP_APPS/maven/current"


# Path adjustments.
$pathBackup = $env:PATH
$env:PATH = "$env:JAVA_HOME/bin;"
$env:PATH += "$env:ECLIPSE_HOME;"
$env:PATH += "$env:IDEA_HOME/bin;"
$env:PATH += "$env:GRADLE_HOME/bin;"
$env:PATH += "$env:MAVEN_HOME/bin;"
$env:PATH += $pathBackup