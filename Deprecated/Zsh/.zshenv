# Make sure the system uses the correct locale.
export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8

# History configuration.
export HISTSIZE=1000
export SAVEHIST=1000
export HISTFILE=~/.history

# Load SSH keys up front.
env=~/.ssh/agent.env

agent_load_env () { test -f "$env" && . "$env" >| /dev/null ; }

agent_start () {
    (umask 077; ssh-agent >| "$env")
    . "$env" >| /dev/null ; }

agent_load_env

# agent_run_state: 0=agent running w/ key; 1=agent w/o key; 2= agent not running
agent_run_state=$(ssh-add -l >| /dev/null 2>&1; echo $?)

if [ ! "$SSH_AUTH_SOCK" ] || [ $agent_run_state = 2 ]; then
    agent_start
    ssh-add ~/.ssh/id_ed25519_gitlab
    ssh-add ~/.ssh/id_ed25519_github
elif [ "$SSH_AUTH_SOCK" ] && [ $agent_run_state = 1 ]; then
    ssh-add ~/.ssh/id_ed25519_gitlab
    ssh-add ~/.ssh/id_ed25519_github
fi

unset env

# Add local binaries to path.
export PATH=$PATH:$HOME/.local/bin:$HOME/.cargo/bin

# Start Starship.
eval "$(starship init zsh)"

# Start Zoxide.
eval "$(zoxide init zsh)"

# Set blurred background for Kitty.
if [[ $(ps --no-header -p $PPID -o comm) =~ '^kitty$' ]]; then
    for wid in $(xdotool search --pid $PPID); do
        xprop -f _KDE_NET_WM_BLUR_BEHIND_REGION 32c -set _KDE_NET_WM_BLUR_BEHIND_REGION 0 -id $wid; done
fi

# Standard aliases
alias dir='exa --long --header --git --classify --icons --all --time-style=long-iso --group-directories-first'
alias cd='z'

# Flatpak aliases.
alias code='flatpak run com.visualstudio.code'

# Aditional movement keys.
bindkey "^[[H" beginning-of-line
bindkey "^[[F" end-of-line
bindkey "^[[3~" delete-char
bindkey "^[[1;3C" forward-word
bindkey "^[[1;3D" backward-word
